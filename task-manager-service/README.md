# TASK MANAGER SERVICE

The back-end service implementation of Task Manager application.

This is a Java Spring Boot application with MySql DB connection.


## Steps to run the solution 

- Start the local MySql instance
- Run "mvn spring-boot:run"
- The api can be accessed via http://localhost:8080/api/
- Test :  http://localhost:8080/api/tasks

