package com.dalt.taskmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskManagerApplication {

	public static void main(final String[] args) {
		SpringApplication.run(TaskManagerApplication.class, args);
	}

}
