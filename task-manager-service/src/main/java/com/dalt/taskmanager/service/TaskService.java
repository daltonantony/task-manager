package com.dalt.taskmanager.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dalt.taskmanager.model.ParentTask;
import com.dalt.taskmanager.model.Task;
import com.dalt.taskmanager.repository.ParentTaskRepository;
import com.dalt.taskmanager.repository.TaskRepository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Resource
    private TaskRepository taskRepository;

    @Resource
    private ParentTaskRepository parentTaskRepository;

    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    public Task findById(final long id) {
        final Optional<Task> task = taskRepository.findById(id);
        return task.orElse(null);
    }

    @Transactional
    public void updateTask(final Task task) {
        taskRepository.save(task);
    }

    @Transactional
    public void addTask(final Task task) {
        if (task.getParentTask() != null) {
            Optional<ParentTask> optParentTask = parentTaskRepository.findById(task.getParentTask().getId());
            optParentTask.ifPresent(task::setParentTask);
        }
        taskRepository.save(task);
    }

    @Transactional
    public void deleteTask(final long id) {
        taskRepository.deleteById(id);
    }

}
